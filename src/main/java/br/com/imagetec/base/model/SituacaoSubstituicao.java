package br.com.imagetec.base.model;

public enum SituacaoSubstituicao {

	EM_ANDAMENTO("Em andamento"),
	PERIODO_DECORRIDO("Período decorrido"),
	AGUARDANDO_INICIO("Aguardando início"),
	INTERROMPIDA("Interrompida");
	
	private String label;

	SituacaoSubstituicao(String label) {
		this.label = label;
	}
	
	public String getLabel() {
		return label;
	}
	
}
