package br.com.imagetec.base.model;

public enum TipoOrigem {
	
	TL("Tô Ligado Diadema"), 
	PE("Processos Eletrônicos"),
	AMBOS(TipoOrigem.TL.getLabel() + " / " + TipoOrigem.PE.getLabel());
	
	private String label;
	
	TipoOrigem(String label) {
		this.label = label;
	}
	
	public String getLabel() {
		return this.label;
	}
	
}
