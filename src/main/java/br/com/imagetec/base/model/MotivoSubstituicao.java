package br.com.imagetec.base.model;

public enum MotivoSubstituicao {

	FERIAS("Férias"),
	CURSO("Curso"),
	AFASTAMENTO("Afastamento"),
	FALTA("Falta"),
	LICENCA("Licença"),
	OUTROS("Outros");
	
	private String label;

	MotivoSubstituicao(String label) {
		this.label = label;
	}
	
	public String getLabel() {
		return this.label;
	}
	
	public String getName() {
		return this.name();
	}
	
}
