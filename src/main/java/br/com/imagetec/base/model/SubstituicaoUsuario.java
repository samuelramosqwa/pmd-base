package br.com.imagetec.base.model;


import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;

import org.joda.time.LocalDate;
import org.springframework.stereotype.Component;

import br.com.imagetec.base.utils.Objects;

@Entity
@Component
@Table(
    name="SubstituicaoUsuario", 
    uniqueConstraints=
    @UniqueConstraint(columnNames={"ID_SUBSTITUIDO", "ID_SUBSTITUTO", "dataInicio", "dataFim", "motivo"})
)
public class SubstituicaoUsuario {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_SUBSTITUTO_USU")
	@SequenceGenerator(name = "SEQ_SUBSTITUTO_USU", sequenceName = "SEQ_SUBSTITUTO_USU", allocationSize = 1)
	private Long id;
	
	@ManyToOne
	@JoinColumn(name = "ID_SUBSTITUIDO")
	private UsuarioInterno substituido;
	
	@ManyToOne
	@JoinColumn(name = "ID_SUBSTITUTO")
	private UsuarioInterno substituto;
	
	@ManyToOne
	@JoinColumn(name = "ID_RESPONSAVEL")
	private UsuarioInterno responsavel;
	
	private Date dataCriacao;
	
	private Date dataInicio;
	private Date dataFim;
	private MotivoSubstituicao motivo;
	private SituacaoSubstituicao situacao;
		
	protected SubstituicaoUsuario(){}
	
	private SubstituicaoUsuario(Long id, UsuarioInterno substituido, UsuarioInterno substituto, UsuarioInterno responsavel, Date dataInicio, Date dataFim,
			MotivoSubstituicao motivo, LocalDate localDateAgora) throws RuntimeException {
	
		Objects.requireNonNull(substituido, "O usuário interno substituido não pode ser nulo!");
		Objects.requireNonNull(substituto, "O usuário interno substituto não pode ser nulo!");
		Objects.requireNonNull(dataInicio, "A data de início não pode ser nula!");
		Objects.requireNonNull(dataFim, "A data fim não pode ser nula!");
		Objects.requireNonNull(motivo, "O motivo da substituição não pode ser nulo!");
		Objects.requireNonNull(responsavel, "O usuário responsavel pela substituição não pode ser nulo!");
		
		LocalDate localDateInicio = new LocalDate(dataInicio);
		LocalDate localDateFim = new LocalDate(dataFim);
		
		if (substituido.equals(substituto))
			throw new IllegalArgumentException("O usuário substituido não pode ser igual ao usuário substituto.");
		
		if (localDateInicio.compareTo(localDateAgora) == -1)
			throw new IllegalArgumentException("A data início não pode ser menor do que a data atual.");
		
		if (localDateInicio.compareTo(localDateFim) == 1)
			throw new IllegalArgumentException("A data início não pode ser maior ou igual a data fim.");
		
		this.substituido = substituido;
		this.substituto = substituto;
		this.responsavel = responsavel;
		this.dataInicio = dataInicio;
		this.dataFim = dataFim;
		this.motivo = motivo;
		this.situacao = SituacaoSubstituicao.AGUARDANDO_INICIO;
		this.dataCriacao = new Date();
		this.id = id;
		
	}
	
	/**
	 * @param agora : variável utilizada para testes unitários
	 * @return
	 * @throws IllegalArgumentException
	 */
	protected static SubstituicaoUsuario create(UsuarioInterno substituido, UsuarioInterno substituto, UsuarioInterno responsavel, Date dataInicio, Date dataFim,
			MotivoSubstituicao motivo, Date agora) throws RuntimeException {
		return new SubstituicaoUsuario(null, substituido, substituto, responsavel, dataInicio, dataFim, motivo, new LocalDate(agora));
	}
	
	public final static SubstituicaoUsuario create(UsuarioInterno substituido, UsuarioInterno substituto, UsuarioInterno responsavel, Date dataInicio, Date dataFim,
			MotivoSubstituicao motivo) throws RuntimeException {
		return new SubstituicaoUsuario(null, substituido, substituto, responsavel, dataInicio, dataFim, motivo, LocalDate.now());
	}
	
	public final static SubstituicaoUsuario create(Long id, UsuarioInterno substituido, UsuarioInterno substituto, UsuarioInterno responsavel, Date dataInicio, Date dataFim,
			MotivoSubstituicao motivo) throws RuntimeException {
		return new SubstituicaoUsuario(id, substituido, substituto, responsavel, dataInicio, dataFim, motivo, LocalDate.now());
	}
	
	@Override
	public String toString() {
		return "SubstituicaoUsuario [id=" + id + ", substituido=" + substituido.getNome() + ", substituto=" + substituto.getNome()
				+ ", responsavel=" + responsavel.getNome() + ", dataCriacao=" + dataCriacao + ", dataInicio=" + dataInicio
				+ ", dataFim=" + dataFim + ", motivo=" + motivo + ", situacao=" + situacao + "]";
	}

	public static LocalDate getDataAgora() {
		return LocalDate.now();
	}

	public UsuarioInterno getSubstituido() {
		return substituido;
	}

	public UsuarioInterno getSubstituto() {
		return substituto;
	}

	public Date getDataInicio() {
		return dataInicio;
	}

	public Date getDataFim() {
		return dataFim;
	}

	public MotivoSubstituicao getMotivo() {
		return motivo;
	}
	
	public Long getId() {
		return id;
	}
	
	public UsuarioInterno getResponsavel() {
		return responsavel;
	}
	
	public Date getDataCriacao() {
		return dataCriacao;
	}
	
	/**
	 * Altera a situação da substituição para Interrompida. Uma substituição só pode ser interrompida se estiver em 
	 * andamento.
	 * @return
	 */
	
	public void interromper(UsuarioInterno responsavel) {
		
		if (getSituacao().equals(SituacaoSubstituicao.EM_ANDAMENTO)){
			this.situacao = SituacaoSubstituicao.INTERROMPIDA;
			this.responsavel = responsavel;
		} else {
			throw new RuntimeException("Erro ao tentar interromper substituição, motivo: substituição deve estar em andamento.");
		}
	}
	
	public SituacaoSubstituicao getSituacao() {
		
		if (situacao != null && situacao.equals(SituacaoSubstituicao.INTERROMPIDA))
			return situacao;
		
		LocalDate agora = LocalDate.now();
		LocalDate inicio = new LocalDate(dataInicio);
		LocalDate fim = new LocalDate(dataFim);
						  
		if ((agora.compareTo(inicio) == 0 || agora.compareTo(inicio) == 1) && // agora é >= inicio &&
		   (agora.compareTo(fim) == 0 || agora.compareTo(fim) == -1)) // agora é <= fim
			return SituacaoSubstituicao.EM_ANDAMENTO;
		
		// se agora é menor do que início
		if (agora.compareTo(inicio) == -1)
			return SituacaoSubstituicao.AGUARDANDO_INICIO;
		
		// se agora é maior que a data fim
		if (agora.compareTo(fim) == 1) 
			return SituacaoSubstituicao.PERIODO_DECORRIDO;
		
		return situacao;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((dataFim == null) ? 0 : dataFim.hashCode());
		result = prime * result + ((dataInicio == null) ? 0 : dataInicio.hashCode());
		result = prime * result + ((motivo == null) ? 0 : motivo.hashCode());
		result = prime * result + ((substituido == null) ? 0 : substituido.hashCode());
		result = prime * result + ((substituto == null) ? 0 : substituto.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SubstituicaoUsuario other = (SubstituicaoUsuario) obj;
		if (dataFim == null) {
			if (other.dataFim != null)
				return false;
		} else if (!dataFim.equals(other.dataFim))
			return false;
		if (dataInicio == null) {
			if (other.dataInicio != null)
				return false;
		} else if (!dataInicio.equals(other.dataInicio))
			return false;
		if (motivo != other.motivo)
			return false;
		if (substituido == null) {
			if (other.substituido != null)
				return false;
		} else if (!substituido.equals(other.substituido))
			return false;
		if (substituto == null) {
			if (other.substituto != null)
				return false;
		} else if (!substituto.equals(other.substituto))
			return false;
		return true;
	}

	public void removerTodosUsuariosRelacionados() {
		substituido = null;
		substituto = null;
		responsavel = null;
	}
}
