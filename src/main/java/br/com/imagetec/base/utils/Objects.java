package br.com.imagetec.base.utils;

public class Objects {

	public static void requireNonNull(Object object, String msg) throws NullPointerException {
		if(object == null) throw new NullPointerException(msg);
	}
}
